package ocean;

import gov.nasa.worldwind.layers.TextureTile;


public interface TileImageGenerator {
  void createTileImage(TextureTile tile);

  String getDataSourceName();
}
