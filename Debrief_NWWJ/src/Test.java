import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.event.RenderingEvent;
import gov.nasa.worldwind.event.RenderingListener;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.globes.EllipsoidalGlobe;
import gov.nasa.worldwind.globes.Globe;
import gov.nasa.worldwind.layers.TextureTile;
import gov.nasa.worldwind.terrain.ZeroElevationModel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureIO;

import ocean.RectangularNormalTessellator;
import ocean.TileImageGenerator;
import ocean.ElevatedTiledImageLayer;
import ocean.TimelineLayer;
import ocean.UnderwaterEffectLayer;

public class Test extends ApplicationTemplate {

  final static int TILE_SIZE = 512;
  final static BufferedImage OCEAN_IMAGE;

  static {
    Configuration.setValue(AVKey.TESSELLATOR_CLASS_NAME,
        RectangularNormalTessellator.class.getName());

    OCEAN_IMAGE = new BufferedImage(TILE_SIZE, TILE_SIZE,
        BufferedImage.TYPE_INT_ARGB);

    int color = new Color(0, 0, 255, 85).getRGB();

    for (int x = 0; x < TILE_SIZE; x++) {
      for (int y = 0; y < TILE_SIZE; y++) {
        OCEAN_IMAGE.setRGB(x, y, color);
      }
    }
  }

  public static class AppFrame extends ApplicationTemplate.AppFrame {
	  /**
	 * 
	 */
	private static final long serialVersionUID = -7323887044388560857L;
	protected TimelineLayer _timelineLayer;
	protected JLabel timeLabel;
	  
    public AppFrame() {
      super(true, true, false);

      final float maxAltitude = 350000;
      final float activeAltitude = 5000;
      
      final UnderwaterEffectLayer fogLayer = new UnderwaterEffectLayer();
      fogLayer.setName("Underwater effect");
      
      final OceanLayer oceanLayer = new OceanLayer();
      //oceanLayer.setOpacity(0.2);
      oceanLayer.setUseTransparentTextures(true);
      oceanLayer.setMaxActiveAltitude(maxAltitude);
     
      _timelineLayer = new TimelineLayer();
      _timelineLayer.setName("Tracks");
      _timelineLayer.addTrack("tracks/boat1a.rep", "objs/HMS Astute.dae");
      _timelineLayer.addTrack("tracks/boat2a.rep", "objs/HMS Daring.dae");
      //_timelineLayer.addTrack("tracks/test1.rep", "objs/GREEK.dae");
      _timelineLayer.addAndInterpolateTrack("tracks/test2.rep", "objs/Lockheed P-3C 2.dae");
      //_timelineLayer.addTrack("tracks/test3.rep", "objs/HMS Daring.dae");
      
      // Add the layer to the model.
      insertBeforeCompass(getWwd(), _timelineLayer);
      insertBeforeCompass(getWwd(), oceanLayer);
      insertBeforeCompass(getWwd(), fogLayer);
      
      
            
      final WorldWindow wwd = this.getWwd();
      
      // Add rendering listener
      wwd.addRenderingListener(new RenderingListener() {		
			@Override
			public void stageChanged(RenderingEvent event) {
				double currentAltitude = wwd.getView().getCurrentEyePosition().getAltitude();
				if (currentAltitude > activeAltitude && currentAltitude < maxAltitude) {
					double blendFactor = activeAltitude / currentAltitude - activeAltitude / maxAltitude;
					//System.out.println(blendFactor);
					oceanLayer.setOpacity(blendFactor);
				}
				//System.out.println(currentAltitude);
				//System.out.println(wwd.getView().getNearClipDistance());
			}
      });
            
      // Update layer panel
      this.getLayerPanel().update(this.getWwd());
      
      // Add view controls selection panel
      this.getLayerPanel().add(makeControlPanel(), BorderLayout.SOUTH);
    }
    
    private JPanel makeControlPanel()
    {
        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
        controlPanel.setBorder(
            new CompoundBorder(BorderFactory.createEmptyBorder(9, 9, 9, 9), new TitledBorder("Timeline Controls")));
        controlPanel.setToolTipText("select time from slider");

        
        // Time slider
        JPanel timePanel = new JPanel(new GridLayout(0, 1, 0, 0));
        timePanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        // Label for current time
        timeLabel = new JLabel(_timelineLayer.get_currentDatetime().getTime().toString());
        
        // Slider for min/max time
        long minTime = _timelineLayer.get_minDatetime().getTimeInMillis();
        long maxTime = _timelineLayer.get_maxDatetime().getTimeInMillis();
        long deltaTime = maxTime - minTime;
        long deltaTimeInMinute = deltaTime / (60 * 1000);
        
        System.out.println(deltaTime + " " + deltaTimeInMinute);
        
        JSlider timeSlider = new JSlider(0, (int)deltaTimeInMinute, 0);
        timeSlider.addChangeListener(new ChangeListener()
        {
            public void stateChanged(ChangeEvent event)
            {
            	int currentTimeInMinute = ((JSlider) event.getSource()).getValue();
            	long currentTimeInMlseconde = currentTimeInMinute * 60 * 1000;
            	Calendar currentDateTime = Calendar.getInstance();
            	currentDateTime.setTimeInMillis(_timelineLayer.get_minDatetime().getTimeInMillis() + currentTimeInMlseconde);
            	_timelineLayer.set_currentDatetime(currentDateTime);
            	timeLabel.setText(_timelineLayer.get_currentDatetime().getTime().toString());
                getWwd().redraw();
            }
        });
        timePanel.add(timeLabel);
        timePanel.add(timeSlider);       

        
        controlPanel.add(timePanel);
        return controlPanel;
  	  }
  	}
  
  
  

  static class OceanLayer extends ElevatedTiledImageLayer {
    public OceanLayer() {
      super(createImageGenerator(), TILE_SIZE, createGlobe());
      setPickEnabled(false);
    }

    @Override
    public String toString() {
      return "Oceans";
    }

    static Globe createGlobe() {
      return new OceanGlobe();
    }

    static class OceanGlobe extends EllipsoidalGlobe {
      OceanGlobe() {
        super(Earth.WGS84_EQUATORIAL_RADIUS, Earth.WGS84_POLAR_RADIUS,
            Earth.WGS84_ES, new ZeroElevationModel());

        RectangularNormalTessellator tessellator = new RectangularNormalTessellator(this);
        tessellator.setRegenerateCache(false);

        setTessellator(tessellator);
      }
    }

    static TileImageGenerator createImageGenerator() {
      return new TileImageGenerator() {
        @Override
        public void createTileImage(TextureTile tile) {
          TextureData textureData = TextureIO
              .newTextureData(OCEAN_IMAGE, false);
          tile.setTextureData(textureData);
        }

        @Override
        public String getDataSourceName() {
          return "Ocean Surface";
        }
      };
    }
  }

  public static void main(String[] args) {
    ApplicationTemplate.start("Ocean", AppFrame.class);
  }

}
