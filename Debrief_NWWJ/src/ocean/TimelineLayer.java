package ocean;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.media.opengl.GL;

import net.java.joglutils.model.ModelLoadException;

import com.sun.opengl.util.j2d.TextRenderer;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.formats.models.collada.ColladaModel;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Matrix;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.Polyline;
import ocean.TrackReader.Informations;

public class TimelineLayer extends RenderableLayer {
	private TextRenderer _textRender = new TextRenderer(new Font("SansSerif", Font.BOLD, 16));
    private float _size = 100; //Object size
    private float _closeViewDistance = 150000;
   
	private class Track {
		public HashMap<String, Informations> timePositions;
		public ColladaModel model3D;
	}
	
	private final List<Track> _tracks = new ArrayList<TimelineLayer.Track>();
	private final TrackReader _trackReader = new TrackReader();
	private Calendar _minDatetime = null;
	private Calendar _maxDatetime = null;
	private Calendar _currentDatetime = Calendar.getInstance();
	
	public Calendar get_minDatetime() {
		return _minDatetime;
	}

	public void set_minDatetime(Calendar _minDatetime) {
		this._minDatetime = _minDatetime;
	}

	public Calendar get_maxDatetime() {
		return _maxDatetime;
	}

	public void set_maxDatetime(Calendar _maxDatetime) {
		this._maxDatetime = _maxDatetime;
	}

	public Calendar get_currentDatetime() {
		return _currentDatetime;
	}

	public void set_currentDatetime(Calendar _currentDatetime) {
		this._currentDatetime = _currentDatetime;
	}	
	
	
	public void addTrack(String filename, String daefile) {
		Track track = new Track();
		track.timePositions = new HashMap<String, TrackReader.Informations>();
		
		List<Informations> infos = new ArrayList<TrackReader.Informations>();
		List<Polyline> p = _trackReader.constructPolylineFromFile(filename, infos);
		for(int i=0; i<p.size(); i++) {
		    this.addRenderable(p.get(i));
		}
		
		//Searching for min-max dates	
		if (_minDatetime == null && _maxDatetime == null) { //First initiation
			Calendar aDate = infos.get(0).date;	
			_minDatetime = aDate;
			_maxDatetime = aDate;			
		}
		
		for(int i=0; i<infos.size(); i++) {
			Calendar aDate = infos.get(i).date;
			if(_minDatetime.after(aDate))
				_minDatetime = aDate;
			
			if(_maxDatetime.before(aDate))
				_maxDatetime = aDate;
			
			track.timePositions.put(aDate.getTime().toString(), infos.get(i));
//			System.out.println("date: " + aDate.getTime().toString());
		}	
		
		
		//Add track to tracks list
		_tracks.add(track);
		
		System.out.println("Min: " + _minDatetime.getTime().toString());
		System.out.println("Max: " + _maxDatetime.getTime().toString());
//		System.out.println(track.timePositions.toString());
		System.out.println(filename);
		setDatetime(_minDatetime);
		
		//Add 3D model
		try {
			track.model3D = new ColladaModel(daefile, Position.fromDegrees(0, 0, 200));
			track.model3D.setAltitudeMode(WorldWind.ABSOLUTE);
			track.model3D.setConstantSize(true);
			track.model3D.setSize(this._size);
		} catch (ModelLoadException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void addAndInterpolateTrack(String filename, String daefile) {
		Track track = new Track();
		track.timePositions = new HashMap<String, TrackReader.Informations>();
		
		List<Informations> infos = new ArrayList<TrackReader.Informations>();
		List<Polyline> p = _trackReader.constructPolylineFromFile(filename, infos);
		for(int i=0; i<p.size(); i++) {
		    this.addRenderable(p.get(i));
		}
		
		//Searching for min-max dates	
		if (_minDatetime == null && _maxDatetime == null) { //First initiation
			Calendar aDate = infos.get(0).date;	
			_minDatetime = aDate;
			_maxDatetime = aDate;			
		}
		
		long minTime = this.get_minDatetime().getTimeInMillis();
        long maxTime = this.get_maxDatetime().getTimeInMillis();
        long deltaTime = maxTime - minTime;
        int deltaTimeInMinute = (int) (deltaTime / (60 * 1000));
        
        int interpolateStep = deltaTimeInMinute / (infos.size()-1);
        int remainedInfosCount = deltaTimeInMinute - (interpolateStep * (infos.size()-1));        
        
        //Interpolate
        List<Informations> newInfos = new ArrayList<TrackReader.Informations>();
        Informations interpolatedInfos = new TrackReader.Informations();
        for(int i=0; i<infos.size() - 1; i++) {
        	Informations currentInfos = infos.get(i);
        	Informations nextInfos = infos.get(i + 1);
        	
        	for(int j=0; j<interpolateStep; j++) {
        		interpolatedInfos = new TrackReader.Informations();
        		interpolatedInfos.color = currentInfos.color;
        		interpolatedInfos.name = currentInfos.name;
        		interpolatedInfos.speed = currentInfos.speed;
        		interpolatedInfos.heading = currentInfos.heading;
        		double step = interpolateStep - 1;
        		interpolatedInfos.depth = ((step - (double)j)/step) * currentInfos.depth + 
										((double)j/step) * nextInfos.depth;        		
        		interpolatedInfos.latitude = ((step - (double)j)/step) * currentInfos.latitude + 
        									((double)j/step) * nextInfos.latitude;
        		interpolatedInfos.longitude = ((step - (double)j)/step) * currentInfos.longitude + 
        									((double)j/step) * nextInfos.longitude;
        		
        		
        		newInfos.add(interpolatedInfos);
        	}
		}
        
        //Fill last elements
        for(int i=0; i<remainedInfosCount; i++) {
        	newInfos.add(interpolatedInfos);
        	
        }
       
//        System.out.println("DELTA TIME: " + deltaTimeInMinute + " " + interpolateStep + " " + newInfos.size() + " " + remainedInfosCount);
        
		
		for(int i=0; i<deltaTimeInMinute; i++) {
			Calendar aDate = infos.get(0).date;	
			aDate.setTimeInMillis(minTime + (i * 60000));
						
			
			Informations newInfo = newInfos.get(i);
			//Update newInfo
			newInfo.date = aDate;
			
			track.timePositions.put(aDate.getTime().toString(), newInfo);
//			System.out.println("date: " + aDate.getTime().toString());
		}	
		
		
		//Add track to tracks list
		_tracks.add(track);
		
		
		//Add 3D model
		try {
			track.model3D = new ColladaModel(daefile, Position.fromDegrees(0, 0, 200));
			track.model3D.setAltitudeMode(WorldWind.ABSOLUTE);
			track.model3D.setConstantSize(true);
			track.model3D.setSize(this._size);
		} catch (ModelLoadException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void setDatetime(Calendar datetime) {
		_currentDatetime.setTime(datetime.getTime());
	}
	
	@Override
	public void doRender(DrawContext dc) {
		super.doRender(dc);
		
		//Drawing cylinder from the current date time
		Position eyePos = dc.getView().getEyePosition();
	    if (eyePos == null)
	      return;
	    float alt = (float) eyePos.getElevation();
	    
	    //Only draw detail when zooming close enough
	    if(alt < this._closeViewDistance) {
	    
			GL gl = dc.getGL();
	
	        int attrMask = GL.GL_CURRENT_BIT | GL.GL_COLOR_BUFFER_BIT;
	
	        gl.glPushAttrib(attrMask);
	        dc.beginStandardLighting();
	        gl.glEnable(GL.GL_NORMALIZE);
	        gl.glEnable(GL.GL_DEPTH_TEST);
	        gl.glDisable(GL.GL_BLEND);
	        
	        //For each tracks drawing a shape that represent a boat at current time
	        for(int i=0; i<_tracks.size(); i++) {
	        	Track aTrack = _tracks.get(i);
	        	Informations infos = aTrack.timePositions.get(_currentDatetime.getTime().toString());
	        	//drawLabel(dc, "hello world", 0, 0);
	             
	//        	System.out.println(_currentDatetime.getTime().toString());
	        	if (infos != null) {
	        		gl.glMatrixMode(GL.GL_MODELVIEW);
	
	                Matrix matrix = dc.getGlobe().computeSurfaceOrientationAtPosition(Position.fromDegrees(infos.latitude, infos.longitude, -infos.depth));
	                matrix = dc.getView().getModelviewMatrix().multiply(matrix);
	
	                double[] matrixArray = new double[16];
	                matrix.toArray(matrixArray, 0, false);
	                gl.glLoadMatrixd(matrixArray, 0);
	                
	                //Draw 3D representation
//		            gl.glEnable(GL.GL_LIGHTING);
//		            gl.glColorMaterial ( GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT );
//		            gl.glEnable ( GL.GL_COLOR_MATERIAL );
//		            gl.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE,  GL.GL_MODULATE);
	//	            
//		            gl.glPushMatrix();
//		            gl.glColor3f(infos.color.getRed(), infos.color.getGreen(), infos.color.getBlue());
	//	            gl.glRotatef(-infos.heading, 0.0f, 0.0f, 1.0f);
//		            gl.glScaled(this.size, this.size, this.size);
//	                drawUnitCube(dc);
	                //drawUnitCylinder(dc);
//	                gl.glPopMatrix();
	                

	                aTrack.model3D.lightAmbient[0] = infos.color.getRed() / 255;
	                aTrack.model3D.lightAmbient[1] = infos.color.getGreen() / 255;
	                aTrack.model3D.lightAmbient[2] = infos.color.getBlue() / 255;
	                
	                aTrack.model3D.model_ambient[0] = infos.color.getRed() / 255;
	                aTrack.model3D.model_ambient[1] = infos.color.getGreen() / 255;
	                aTrack.model3D.model_ambient[2] = infos.color.getBlue() / 255;
	                //aTrack.model3D.setSize(this.size);
	                aTrack.model3D.setYaw(Angle.fromDegrees(-180 + infos.heading));
	                aTrack.model3D.setPosition(Position.fromDegrees(infos.latitude, infos.longitude, -infos.depth));
	                aTrack.model3D.render(dc);
	                	                
	                //Draw label
	                //Compute screen-space coordinates for the current position
	                Vec4 pos = dc.getGlobe().computePointFromPosition(Position.fromDegrees(infos.latitude, infos.longitude, -infos.depth + this._size));
	                Vec4 screenPos = dc.getView().project(pos);
	                _textRender.beginRendering(dc.getDrawableWidth(), dc.getDrawableHeight());
	                _textRender.setColor(infos.color.getRed(), infos.color.getGreen(), infos.color.getBlue(), 1);
	                _textRender.draw(infos.name, (int)screenPos.x(), (int)screenPos.y());
	                _textRender.endRendering();
	        	}
	        }
	        
	//        gl.glDisable(GL.GL_LIGHTING);
	        gl.glDisable(GL.GL_DEPTH_TEST);
	        dc.endStandardLighting();
	        gl.glPopAttrib();
        
	    }
	}
	
	/**
     * Draw a unit cube, using the active modelview matrix to orient the shape.
     *
     * @param dc Current draw context.
     */
    protected void drawUnitCube(DrawContext dc) {
        // Vertices of a unit cube, centered on the origin.
        float[][] v = {{-0.5f, 0.5f, -0.5f}, {-0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, 0.5f}, {0.5f, 0.5f, -0.5f},
            {-0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, 0.5f}, {0.5f, -0.5f, -0.5f}, {-0.5f, -0.5f, -0.5f}};

        // Array to group vertices into faces
        int[][] faces = {{0, 1, 2, 3}, {2, 5, 6, 3}, {1, 4, 5, 2}, {0, 7, 4, 1}, {0, 7, 6, 3}, {4, 7, 6, 5}};

        // Normal vectors for each face
        float[][] n = {{0, 1, 0}, {1, 0, 0}, {0, 0, 1}, {-1, 0, 0}, {0, 0, -1}, {0, -1, 0}};

        // Note: draw the cube in OpenGL immediate mode for simplicity. Real applications should use vertex arrays
        // or vertex buffer objects to achieve better performance.
        GL gl = dc.getGL();
        gl.glBegin(GL.GL_LINE_STRIP);
        try {
            for (int i = 0; i < faces.length; i++) {
                gl.glNormal3f(n[i][0], n[i][1], n[i][2]);

                for (int j = 0; j < faces[0].length; j++) {
                    gl.glVertex3f(v[faces[i][j]][0], v[faces[i][j]][1], v[faces[i][j]][2]);
                }
            }
        }
        finally {
            gl.glEnd();
        }
    }
   
}
